### Mybatis Generator

#### 优化项
1. 修改生成的xml文件中空格的缩进
2. 修改生成的pojo属性之间以及方法之间的换行
3. 修改是否合并生成的xml文件
4. 修改添加自定义注释
5. 修改namespace默认生成方式
6. 修改java类型与jdbc类型之间解析问题
7. 修改生成的xml文件元素之间的换行
8. 修改默认不生成文件夹问题
9. dao层增加了一个方法```selectAllSelective```
10. 可以配置生成```service```层

> 参考: http://www.blogjava.net/bolo/archive/2015/03/20/423683.html

