package org.mybatis.generator.extend.servicegenerator;

import org.mybatis.generator.api.IntrospectedColumn;
import org.mybatis.generator.api.dom.java.*;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * service实现查询方法构造
 * 
 * @author tangdelong 2015年5月22日
 */
public class SelectByPrimaryKeyMethodOfServiceGenerator extends AbstractJavaServiceMethodGenerator {

	//private boolean isSimple;

	public SelectByPrimaryKeyMethodOfServiceGenerator(boolean isSimple) {
		super();
		//this.isSimple = isSimple;
	}

	@Override
	public void addServiceElements(TopLevelClass topLevelClass) {
		Set<FullyQualifiedJavaType> importedTypes = new TreeSet<FullyQualifiedJavaType>();
		Method method = new Method();
		method.setVisibility(JavaVisibility.PUBLIC);

		FullyQualifiedJavaType returnType = introspectedTable.getRules().calculateAllFieldsClass();
		method.setReturnType(returnType);
		importedTypes.add(returnType);
		method.addAnnotation("@Override");
		method.setName(introspectedTable.getSelectByPrimaryKeyStatementId());

		List<IntrospectedColumn> introspectedColumns = introspectedTable.getPrimaryKeyColumns();
		for (IntrospectedColumn introspectedColumn : introspectedColumns) {
			FullyQualifiedJavaType type = introspectedColumn.getFullyQualifiedJavaType();
			importedTypes.add(type);
			Parameter parameter = new Parameter(type, introspectedColumn.getJavaProperty());
			method.addParameter(parameter);
			// 方法内容
			FullyQualifiedJavaType mapper = new FullyQualifiedJavaType(introspectedTable.getMyBatis3JavaMapperType());
			StringBuffer bodyLineSB = new StringBuffer();
			bodyLineSB.append("return ");
			String shortName = mapper.getShortName();
			shortName = shortName.substring(0, 1).toLowerCase() + shortName.substring(1);
			bodyLineSB.append(shortName + "." + introspectedTable.getSelectByPrimaryKeyStatementId() + "("+introspectedColumn.getJavaProperty()+");");
			method.addBodyLine(bodyLineSB.toString());
		}
		method.addJavaDocLine("/** 根据主键查询 */");
		context.getCommentGenerator().addGeneralMethodComment(method, introspectedTable);

		if (context.getPlugins().clientSelectByPrimaryKeyMethodGenerated(method, topLevelClass, introspectedTable)) {
			topLevelClass.addImportedTypes(importedTypes);
			topLevelClass.addMethod(method);
		}
	}

}
