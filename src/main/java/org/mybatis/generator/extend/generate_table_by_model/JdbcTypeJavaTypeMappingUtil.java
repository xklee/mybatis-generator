package org.mybatis.generator.extend.generate_table_by_model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lixiangke
 * @date 2017/11/30
 */
public class JdbcTypeJavaTypeMappingUtil {

    private static Map<String, String> mappingInfo = new HashMap<>();

    static {
        mappingInfo.put("java.lang.Integer", "int");
        mappingInfo.put("java.lang.Long", "bigint");
        mappingInfo.put("java.math.BigDecimal", "decimal");
        mappingInfo.put("java.util.Date", "datetime");
        mappingInfo.put("java.lang.String", "varchar");
    }

    public static String getJdbcType(String javaType) {
        String jdbcType = mappingInfo.get(javaType);
        if (jdbcType == null) {
            throw new RuntimeException("没有对应的JDBC类型");
        }
        return jdbcType;
    }

    public static String getJdbcTypeLength(String javaType) {
        if ("int".equals(javaType)) {
            return "11";
        } else if ("bigint".equals(javaType)) {
            return "20";
        } else if ("decimal".equals(javaType)) {
            return "50, 20";
        } else if ("datetime".equals(javaType)) {
            return "0";
        } else if ("varchar".equals(javaType)) {
            return "255";
        } else {
            throw new RuntimeException("没有对应的JDBC类型");
        }
    }
}
