package org.mybatis.generator.extend.interfacegenerator;

import org.mybatis.generator.config.PropertyHolder;

/**
 * 业务层接口生成对应的配置，在配置文件中配置<br>
 * @author lixiangke
 */
public class JavaInterfacesGeneratorConfiguration extends PropertyHolder{
	private String targetPackage;

    private String targetProject;

	public String getTargetPackage() {
		return targetPackage;
	}

	public void setTargetPackage(String targetPackage) {
		this.targetPackage = targetPackage;
	}

	public String getTargetProject() {
		return targetProject;
	}

	public void setTargetProject(String targetProject) {
		this.targetProject = targetProject;
	}
}