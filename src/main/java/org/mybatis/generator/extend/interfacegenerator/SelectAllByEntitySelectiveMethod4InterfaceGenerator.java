/*
 *  Copyright 2009 The Apache Software Foundation
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
package org.mybatis.generator.extend.interfacegenerator;

import org.mybatis.generator.api.dom.java.*;
import org.mybatis.generator.codegen.mybatis3.javamapper.elements.AbstractJavaMapperMethodGenerator;

import java.util.Set;
import java.util.TreeSet;

/**
 * 
 * @author iron will
 * 
 */
public class SelectAllByEntitySelectiveMethod4InterfaceGenerator extends AbstractJavaMapperMethodGenerator {

    public SelectAllByEntitySelectiveMethod4InterfaceGenerator(boolean isSimple) {
        super();
    }

    @Override
    public void addInterfaceElements(Interface interfaze) {
        Set<FullyQualifiedJavaType> importedTypes = new TreeSet<FullyQualifiedJavaType>();
        importedTypes.add(new FullyQualifiedJavaType("java.util.List"));

        Method method = new Method();
        method.setVisibility(JavaVisibility.PUBLIC);
        String rt = introspectedTable.getRules().calculateAllFieldsClass().getShortName();
		FullyQualifiedJavaType returnType = new FullyQualifiedJavaType("List<" + rt + ">");
		method.setReturnType(returnType);
        
        /*FullyQualifiedJavaType returnType = FullyQualifiedJavaType.getNewListInstance();
        FullyQualifiedJavaType listType;
        listType = new FullyQualifiedJavaType(introspectedTable.getBaseRecordType());
        importedTypes.add(listType);
        returnType.addTypeArgument(listType);
        method.setReturnType(returnType);*/
        
        importedTypes.add(returnType);

        method.setName(introspectedTable.getSelectAllSelectiveStatementId());
        
        FullyQualifiedJavaType parameterType = new FullyQualifiedJavaType(introspectedTable.getBaseRecordType());
        importedTypes.add(parameterType);
        method.addParameter(new Parameter(parameterType, "record")); //$NON-NLS-1$
        
        FullyQualifiedJavaType parameterType2 = new FullyQualifiedJavaType("Integer");
		FullyQualifiedJavaType parameterType3 = new FullyQualifiedJavaType("Integer");
		method.addParameter(new Parameter(parameterType2, "pageNum")); //$NON-NLS-1$
		method.addParameter(new Parameter(parameterType3, "pageSize")); //$NON-NLS-1$
        
        addMapperAnnotations(interfaze, method);

        context.getCommentGenerator().addGeneralMethodComment(method,
                introspectedTable);

        if (context.getPlugins().clientSelectByPrimaryKeyMethodGenerated(
                method, interfaze, introspectedTable)) {
            interfaze.addImportedTypes(importedTypes);
            interfaze.addMethod(method);
        }
    }

    public void addMapperAnnotations(Interface interfaze, Method method) {
        return;
    }
}
