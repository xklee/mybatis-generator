package org.mybatis.generator.extend.all_table_generator;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.mybatis.generator.extend.all_table_generator.mappers.TableNamesMapper;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @author lixiangke
 * @date 2017/11/22
 */
public class TableNamesReader {

    /**
     * 数据库表明集合
     */
    public static List<String> listTableNames(String dbName) throws IOException {
        InputStream mbConfig = new ClassPathResource("/mybatis-config.xml").getInputStream();
        SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(mbConfig);
        SqlSession session = sqlSessionFactory.openSession();
        TableNamesMapper mapper = session.getMapper(TableNamesMapper.class);
        return mapper.listTableName(dbName);
    }
}
