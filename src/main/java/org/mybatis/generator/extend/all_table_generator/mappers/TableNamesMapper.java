package org.mybatis.generator.extend.all_table_generator.mappers;

import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author lixiangke
 * @date 2017/11/22
 */
public interface TableNamesMapper {
    @Select("select table_name from information_schema.tables where table_schema = #{dbName}")
    List<String> listTableName(String dbName);
}
