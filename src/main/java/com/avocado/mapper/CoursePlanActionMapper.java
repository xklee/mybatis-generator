package com.avocado.mapper;

import com.avocado.entity.CoursePlanAction;
import java.util.List;

public interface CoursePlanActionMapper {
    CoursePlanAction selectByPrimaryKey(Long id);

    int insertSelective(CoursePlanAction record);

    int updateByPrimaryKeySelective(CoursePlanAction record);

    int deleteByPrimaryKey(Long id);

    List<CoursePlanAction> selectAllSelective(CoursePlanAction record);
}