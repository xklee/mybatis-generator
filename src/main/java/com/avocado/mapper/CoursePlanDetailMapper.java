package com.avocado.mapper;

import com.avocado.entity.CoursePlanDetail;
import java.util.List;

public interface CoursePlanDetailMapper {
    CoursePlanDetail selectByPrimaryKey(Long id);

    int insertSelective(CoursePlanDetail record);

    int updateByPrimaryKeySelective(CoursePlanDetail record);

    int deleteByPrimaryKey(Long id);

    List<CoursePlanDetail> selectAllSelective(CoursePlanDetail record);
}