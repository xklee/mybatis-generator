package com.avocado.entity;

import java.util.Date;

/**
 * 
 * 
 * @author lixiangke
 * @version 1.0 2018-10-21
 */
public class CoursePlanDetail {

    private Long id;

    /**
     * course_plan_id
     */
    private Integer coursePlanId;

    private Long actionGroupId;

    /**
     * 训练名称
     */
    private String name;

    /**
     * 强度
     */
    private Double intension;

    /**
     * 重量
     */
    private Double weight;

    /**
     * 组数
     */
    private Integer groupNum;

    /**
     * 次数
     */
    private Integer cycleNum;

    /**
     * 持续时间（分）
     */
    private String time;

    /**
     * 描述
     */
    private String description;

    /**
     * 顺序
     */
    private Integer ordering;

    private Date createTime;


}