package com.avocado.entity;

import java.util.Date;

/**
 * 
 * 
 * @author lixiangke
 * @version 1.0 2018-10-21
 */
public class CoursePlanAction {

    private Long id;

    /**
     * 课程计划编号
     */
    private Long planId;

    /**
     * 动作类型名称
     */
    private String name;

    /**
     * 强度分数
     */
    private Double weight;

    /**
     * 循环次数
     */
    private Integer cycleNum;

    /**
     * 每组动作休息时间（分）不填页面不展示
     */
    private String restTime;

    /**
     * 背景图地址
     */
    private String bg;

    private Date createTime;

    private Date updateTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlanId() {
        return planId;
    }

    public void setPlanId(Long planId) {
        this.planId = planId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Integer getCycleNum() {
        return cycleNum;
    }

    public void setCycleNum(Integer cycleNum) {
        this.cycleNum = cycleNum;
    }

    public String getRestTime() {
        return restTime;
    }

    public void setRestTime(String restTime) {
        this.restTime = restTime == null ? null : restTime.trim();
    }

    public String getBg() {
        return bg;
    }

    public void setBg(String bg) {
        this.bg = bg == null ? null : bg.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}