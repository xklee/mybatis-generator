package com.avocado.service;

import com.avocado.entity.CoursePlanAction;
import java.util.List;

public interface ICoursePlanActionService {
    CoursePlanAction selectByPrimaryKey(Long id);

    int insertSelective(CoursePlanAction record);

    int updateByPrimaryKeySelective(CoursePlanAction record);

    int deleteByPrimaryKey(Long id);

    List<CoursePlanAction> selectAllSelective(CoursePlanAction record, Integer pageNum, Integer pageSize);
}