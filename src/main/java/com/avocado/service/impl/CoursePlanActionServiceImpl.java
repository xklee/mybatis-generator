package com.avocado.service.impl;

import com.avocado.entity.CoursePlanAction;
import com.avocado.mapper.CoursePlanActionMapper;
import com.avocado.service.ICoursePlanActionService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * 
 * @author lixiangke
 * @version 1.0 2018-10-21
 */
@Service
public class CoursePlanActionServiceImpl implements ICoursePlanActionService {

    @Autowired
    private CoursePlanActionMapper coursePlanActionMapper;

    /** 根据主键查询 */
    @Override
    public CoursePlanAction selectByPrimaryKey(Long id) {
        return coursePlanActionMapper.selectByPrimaryKey(id);
    }

    /** 增加，返回影响的行数 */
    @Override
    public int insertSelective(CoursePlanAction record) {
        return coursePlanActionMapper.insertSelective(record);
    }

    /** 根据主键修改该记录 */
    @Override
    public int updateByPrimaryKeySelective(CoursePlanAction record) {
        return coursePlanActionMapper.updateByPrimaryKeySelective(record);
    }

    /** 根据主键删除该记录 */
    @Override
    public int deleteByPrimaryKey(Long id) {
        return coursePlanActionMapper.deleteByPrimaryKey(id);
    }

    /** 根据条件动态查询 */
    @Override
    public List<CoursePlanAction> selectAllSelective(CoursePlanAction record, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);		List<CoursePlanAction> list = coursePlanActionMapper.selectAllSelective(record);		return list;
    }
}