package com.avocado.service.impl;

import com.avocado.entity.CoursePlanDetail;
import com.avocado.mapper.CoursePlanDetailMapper;
import com.avocado.service.ICoursePlanDetailService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * 
 * @author lixiangke
 * @version 1.0 2018-10-21
 */
@Service
public class CoursePlanDetailServiceImpl implements ICoursePlanDetailService {

    @Autowired
    private CoursePlanDetailMapper coursePlanDetailMapper;

    /** 根据主键查询 */
    @Override
    public CoursePlanDetail selectByPrimaryKey(Long id) {
        return coursePlanDetailMapper.selectByPrimaryKey(id);
    }

    /** 增加，返回影响的行数 */
    @Override
    public int insertSelective(CoursePlanDetail record) {
        return coursePlanDetailMapper.insertSelective(record);
    }

    /** 根据主键修改该记录 */
    @Override
    public int updateByPrimaryKeySelective(CoursePlanDetail record) {
        return coursePlanDetailMapper.updateByPrimaryKeySelective(record);
    }

    /** 根据主键删除该记录 */
    @Override
    public int deleteByPrimaryKey(Long id) {
        return coursePlanDetailMapper.deleteByPrimaryKey(id);
    }

    /** 根据条件动态查询 */
    @Override
    public List<CoursePlanDetail> selectAllSelective(CoursePlanDetail record, Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum, pageSize);		List<CoursePlanDetail> list = coursePlanDetailMapper.selectAllSelective(record);		return list;
    }
}