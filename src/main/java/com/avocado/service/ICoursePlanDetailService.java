package com.avocado.service;

import com.avocado.entity.CoursePlanDetail;
import java.util.List;

public interface ICoursePlanDetailService {
    CoursePlanDetail selectByPrimaryKey(Long id);

    int insertSelective(CoursePlanDetail record);

    int updateByPrimaryKeySelective(CoursePlanDetail record);

    int deleteByPrimaryKey(Long id);

    List<CoursePlanDetail> selectAllSelective(CoursePlanDetail record, Integer pageNum, Integer pageSize);
}