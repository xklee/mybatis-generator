package startup;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.internal.DefaultShellCallback;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * 按照generatorConfig.xml中配置的table元素配置生成代码
 * @author lixiangke
 */
public class Mbg4SpecifiedTable {

	public static void main(String[] args) throws Exception {
		List<String> warnings = new ArrayList<String>();
		URL url = ResourceUtils.getURL("classpath:generatorConfig.xml");
		File configFile = new File(url.toURI());
		ConfigurationParser cp = new ConfigurationParser(warnings);
		Configuration config = cp.parseConfiguration(configFile);
		DefaultShellCallback shellCallback = new DefaultShellCallback(true);
		MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, shellCallback, warnings);
		myBatisGenerator.generate(null);
		System.out.println(warnings);
	}
}
