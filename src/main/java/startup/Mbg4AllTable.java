package startup;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.extend.all_table_generator.TableNamesReader;
import org.mybatis.generator.extend.all_table_generator.TempConfigGenerator;
import org.mybatis.generator.internal.DefaultShellCallback;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 为数据库中所有表生成代码</br>
 * @author lixiangke
 * @date 2017/11/3
 */
public class Mbg4AllTable {

    static final String dbName = "avocado";
    static final String prefix = "t_";

    public static void main(String[] args) throws Exception {
        List<String> warnings = new ArrayList<String>();
        List<String> tableNames = TableNamesReader.listTableNames(dbName);
        TempConfigGenerator tempConfigGenerator = new TempConfigGenerator(tableNames, prefix);
        ConfigurationParser cp = new ConfigurationParser(warnings);
        File configFile = new File(tempConfigGenerator.createConfig());
        Configuration config = cp.parseConfiguration(configFile);
        DefaultShellCallback shellCallback = new DefaultShellCallback(true);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, shellCallback, warnings);
        myBatisGenerator.generate(null);
        System.out.println(warnings);
        System.out.println("Temp File Path : " + configFile.getAbsolutePath());
        System.out.println("Delete Temp File : " + configFile.delete());
    }


}
